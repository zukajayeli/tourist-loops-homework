// Tourist informations

let tourists = [];

tourists[0] = {
  name: 'Mark',
  age: 19,
  cities: ['Tbilisi', 'London', 'Roma', 'Berlin'],
  funds: [120, 200, 150, 140],
};

tourists[1] = {
  name: 'Bob',
  age: 21,
  cities: ['Miami', 'Moscow', 'Vienna', 'Riga', 'Kiyiv'],
  funds: [90, 240, 100, 76, 123],
};

tourists[2] = {
  name: 'Sam',
  age: 22,
  cities: ['Tbilisi', 'Budapest', 'Warsaw', 'Vilnius'],
  funds: [118, 95, 210, 236],
};

tourists[3] = {
  name: 'Anna',
  age: 20,
  cities: ['New York', 'Tokyo', 'Athens', 'Sydney'],
  funds: [100, 240, 50, 190],
};

tourists[4] = {
  name: 'Alex',
  age: 23,
  cities: ['Paris', 'Tbilisi', 'Madrid', 'Marsels', 'Minsk'],
  funds: [96, 134, 76, 210, 158],
};

// Calculating legal age  -- Task 2

for (let i = 0; i < tourists.length; i++) {
  if (tourists[i].age >= 21) {
    tourists[i].legalAge = true;
  } else {
    tourists[i].legalAge = false;
  }
}

// Has been in Georgia?  -- Task 3

for (let i = 0; i < tourists.length; i++) {
  for (let x = 0; x < tourists[i].cities.length; x++)
    if (tourists[i].cities[x] == 'Tbilisi') {
      tourists[i].visitedGeo = 'This tourist has been in Tbilisi';
      break;
    } else {
      tourists[i].visitedGeo = 'Nope, this tourist has never seen Tbilisi';
    }
}

// Total funds spent by tourists  -- Task 4

for (let i = 0; i < tourists.length; i++) {
  for (let x = 0; x < tourists[i].funds.length; x++) {
    let sumExpenses = 0;
    sumExpenses += tourists[i].funds[x];
    tourists[i].totalExpense = sumExpenses;
  }
}

// Average expenses -- Task 5

for (let i = 0; i < tourists.length; i++) {
  let averageExpense = tourists[i].totalExpense / tourists[i].funds.length;
  tourists[i].averageExpense = averageExpense;
}

// Most spending tourist -- Task 6
let maxSpent = tourists[0].totalExpense;

for (let i = 0; i < tourists.length; i++) {
  if (tourists[i].totalExpense > maxSpent) {
    maxSpent = tourists[i].totalExpense;
    maxSpentPrint = `${tourists[i].name} has spent the most - USD ${maxSpent}`;
  }
}

console.log(tourists);
console.log(maxSpentPrint);
